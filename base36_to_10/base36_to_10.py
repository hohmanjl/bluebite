#!/usr/bin/env python3
"""Question 1: a small utility to convert base 36 numbers to base 10 int."""

from string import ascii_lowercase, digits

BASE_36_SYMBOLS = '{}{}'.format(digits, ascii_lowercase)
BASE_36_MAP = {s: i for i, s in enumerate(BASE_36_SYMBOLS)}


def base36_to_base10(base_36_value):
    """Convert positive integer base36 values to decimal integers.

    :param base_36_value, string or int
    :return base 36 converted to base 10 int
    :rtype int"""
    base36 = str(base_36_value).lower()
    length = len(base36) - 1
    value = 0

    for i, v in enumerate(base36):
        power = length - i
        try:
            dec_value = BASE_36_MAP[v]
        except KeyError:
            raise ValueError(
                'Invalid symbol encountered: "{}". Base 36 positive integer '
                'values only.'.format(v)
            )

        value += dec_value * 36**power

    return value


if __name__ == '__main__':
    # prints 10130797
    print('{} -> {}'.format('614z1', base36_to_base10('614z1')))
    # prints 31807670
    print('{} -> {}'.format('ixqye', base36_to_base10('ixqye')))
