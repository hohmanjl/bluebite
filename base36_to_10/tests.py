#!/usr/bin/env python3
"""Small sample of tests. By no means complete."""

import unittest
from collections import namedtuple

from base36_to_10 import (
    base36_to_base10,
    BASE_36_SYMBOLS,
    BASE_36_MAP,
)

TestCase = namedtuple('TestCase', ['input', 'expected'])


class Base36To10Test(unittest.TestCase):
    def test_base_36_symbol_constants(self):
        self.assertEqual(
            BASE_36_SYMBOLS,
            '0123456789abcdefghijklmnopqrstuvwxyz'
        )

    def test_base_36_map_constants(self):
        test_cases = [
            ('0', 0),
            ('1', 1),
            ('9', 9),
            ('a', 10),
            ('z', 35),
        ]
        fail_msg = 'Error! Symbol {} did not map to {}.'
        for tc in test_cases:
            symbol = tc[0]
            value = tc[1]
            self.assertEqual(
                BASE_36_MAP[symbol],
                value,
                fail_msg.format(symbol, value)
            )

    def test_basic_values(self):
        test_cases = [
            TestCase('0', 0),
            TestCase('1', 1),
            TestCase('9', 9),
            TestCase('a', 10),
            TestCase('z', 35),
        ]
        fail_msg = 'Error! Input {} did not map to {}.'
        for tc in test_cases:
            value = base36_to_base10(tc.input)

            self.assertEqual(
                value,
                tc.expected,
                fail_msg.format(tc.input, tc.expected)
            )

    def test_basic_values_to_cmp(self):
        test_cases = [
            '0',
            '1',
            '9',
            'a',
            'z',
        ]
        fail_msg = "Error! Output {} did not match int('{}', 36) == {}."
        for tc in test_cases:
            value = base36_to_base10(tc)
            cmp_value = int(tc, 36)

            self.assertEqual(
                value,
                cmp_value,
                fail_msg.format(value, tc, cmp_value)
            )

    def test_values(self):
        test_cases = [
            TestCase('614z1', 10130797),
            TestCase('ixqye', 31807670),
        ]
        fail_msg = "Error! Output {} did not match int('{}', 36) == {}."
        for tc in test_cases:
            value = base36_to_base10(tc.input)
            cmp_value = int(tc.input, 36)

            self.assertEqual(
                value,
                cmp_value,
                fail_msg.format(value, tc.input, cmp_value)
            )

    def test_negative(self):
        tc = '-4z3'
        with self.assertRaises(ValueError):
            base36_to_base10(tc)

    def test_float(self):
        tc = '-4z3.q9'
        with self.assertRaises(ValueError):
            base36_to_base10(tc)

    def test_case_insensitive(self):
        tc_upper = '43Q9ZI'
        tc_lower = '43q9zi'

        self.assertEqual(
            base36_to_base10(tc_upper),
            base36_to_base10(tc_lower)
        )

        self.assertEqual(
            base36_to_base10(tc_upper),
            int(tc_upper, 36)
        )


if __name__ == '__main__':
    unittest.main()
