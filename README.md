# Blue Bite Coding Challenge

* Question 1 - `base36_to_10` module.
* Question 2 - `question2.sql`.
```
-- Answer. See question2.sql for more details.
-- to see the campaigns result as ordered list, postgres 9.0+ only. Otherwise we have to use subquery
select string_agg(distinct(c.name), ',' order by c.name) as venue_type__campaign
from campaign_tag ct
join tags t
  on ct.tag_id = t.id
join campaigns c
  on ct.campaign_id = c.id
where t.venue_type_id in (1, 3);
```
* Question 3 - `abstractband` module.

# Meta

By: James Hohman

Date: 4/9/2018

---

# Installation

This should run with Python 3 standard libraries.

1. Run tests.

From *inside each module* you can use `unittest`'s test discovery or just execute the `test.py` file directly.

```
$ cd base36_to_10
$ python -m unittest discover -v
test_base_36_map_constants (tests.Base36To10Test) ... ok
test_base_36_symbol_constants (tests.Base36To10Test) ... ok
test_basic_values (tests.Base36To10Test) ... ok
test_basic_values_to_cmp (tests.Base36To10Test) ... ok
test_case_insensitive (tests.Base36To10Test) ... ok
test_float (tests.Base36To10Test) ... ok
test_negative (tests.Base36To10Test) ... ok
test_values (tests.Base36To10Test) ... ok

----------------------------------------------------------------------
Ran 8 tests in 0.003s

OK
```
.
```
$ cd abstractband
$ python -m unittest discover -v
test_cannot_instantiate_abstract (tests.AbstractBandTest) ... ok
test_genre_constructor (tests.SubClassTest) ... ok
test_genre_property_setter (tests.SubClassTest) ... ok
test_red_hot_chili_peppers_constructor (tests.SubClassTest) ... ok
test_rolling_stones_constructor (tests.SubClassTest) ... ok

----------------------------------------------------------------------
Ran 5 tests in 0.001s

OK
```

# Additional questions

## QUESTION 1

*You need to convert a float to 8:8 fixed point notation for a project you’re working on. You find an established Temperature library written in Python that will do the conversion for you, alongside an extensive list of other functionality. Would you import the library or write the conversion yourself? Explain.*

I wasn't sure what fixed point notation was, so I did some searching and found a good paper on the subject: [FixedPointRepresentationFractionalMath.pdf](http://darcy.rsgc.on.ca/ACES/ICE4M/FixedPoint/FixedPointRepresentationFractionalMath.pdf). While this could be implemented, seems like there is a high potential to have bugs in the implementation based on the steps involved and different operators. Since the effort to reproduce the module seems high, and the confidence in the final product is low, the most prudent course of action would be to simply use the Temperature module. Assuming it is a maintained open source project, then that means there are many more sets of eyes on this codebase, which means more exposure to catch defects.

Furthermore, the implementation of some feature is outside of the domain of the problem at hand. For instance, I don't worry about how someone implemented `int` or `float` in Python, I trust that the package has been designed, tested, and defects are reported and worked on. Thus, the work required to build an implementation when someone has already done that exploratory work is likely not worth it, barring unmaintained code or unresolved defects. The best course of action would likely be to fork the project and see if we can factor out the core fixed point functionality into its own module. This would reduce package dependencies and provide a separation of concerns to the library. Temperature library should do temperature stuff, fixed point module should do fixed point stuff, and our module should do our stuff.

## QUESTION 2

*You ended up finishing your tasks for this week’s sprint early. You figure that you have enough time to code one of the additional features from the backlog. Do you code the additional feature or review the code you already wrote? Explain.*

Depends what you mean by "reviewed?" A developer should always proof read his code in its entirety by diffing the commit. This is one type of review. Code is not finished until it has been reviewed and tested. Therefore, until that is done, the code is not finished. Once the code is reviewed (by the developer), tested, and committed, then pluck extra stories from the backlog.

If we are talking about a code review from a peer, then in that case the work is held up by another person's schedule. In that case, if the "proof read review" is complete, code is tested and checked in, then by all means pluck from the backlog until the peer reviewer is available.

## QUESTION 3

*You are assigned a personal task to create a small-scale API for a client and
requirements outline a model that needs to store loosely defined data, with no strictly required fields. The team normally writes in MySQL, but you determine that MongoDB, a database you are familiar with, will clearly be more efficient. Would you use MySQL or MongoDB for the API? Explain.*

There are several considerations here. Asking the team to support additional technology with the learning costs involved. Adding a new layer of technology to support to the stack. Increasing surface area for attacks.

Being able to leverage technology that is a better fit to solve a problem at hand is ideal. But we would have to estimate what extra costs are associated with adopting an additional technology. If this is a one off, small scale project where MongoDB will never be used again, it may not be worth it. For instance, perhaps an alternative solution would be to use MySQL but a JSON Field to store the schema-less data. This way we provide a solution that has flexibility to support schema less models while fitting into our existing architecture with minimal effort and maintaining the current stack.

However, if we foresee a potential need to serve future clients with schema less models, then adopting MongoDB now and working out the kinks may be the better course of action. This will allow us to let the technology do the work it was built to do, and it may be worth it to invest in a more diversified tech stack to be able to deal with different classes of problems in the most efficient manner possible.
