#!/usr/bin/env python3
"""Small sample of tests. By no means complete."""

import unittest

from abstractband import (
    Band,
    RollingStones,
    RedHotChiliPeppers,
)


class AbstractBandTest(unittest.TestCase):
    def test_cannot_instantiate_abstract(self):
        with self.assertRaises(TypeError):
            Band()


class SubClassTest(unittest.TestCase):
    def test_rolling_stones_constructor(self):
        rs = RollingStones()
        self.assertEqual(rs.genre, Band.DEFAULT_GENRE)
        self.assertEqual(rs.name, 'Rolling Stones')

    def test_red_hot_chili_peppers_constructor(self):
        rhcp = RedHotChiliPeppers()
        self.assertEqual(rhcp.genre, Band.DEFAULT_GENRE)
        self.assertEqual(rhcp.name, 'Red Hot Chili Peppers')

    def test_genre_property_setter(self):
        classes = [RollingStones, RedHotChiliPeppers]

        expected = 'funky'
        for Class in classes:
            instance = Class()
            pre_genre = instance.genre
            instance.genre = expected
            post_genre = instance.genre

            self.assertNotEqual(pre_genre, post_genre)
            self.assertEqual(post_genre, expected)

    def test_genre_constructor(self):
        classes = [RollingStones, RedHotChiliPeppers]

        expected = 'funky'
        for Class in classes:
            instance = Class(expected)

            self.assertEqual(instance.genre, expected)


if __name__ == '__main__':
    unittest.main()
