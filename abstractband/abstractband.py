#!/usr/bin/env python3
"""Question 3 Abstract class implementation."""

from abc import ABC, abstractmethod


class Band(ABC):
    DEFAULT_GENRE = 'rock'
    DEFAULT_NAME = 'Unknown'

    @abstractmethod
    def __init__(self, genre=DEFAULT_GENRE, name=DEFAULT_NAME):
        self._genre = genre
        self._name = name

    def __repr__(self):
        return "{}('{}', '{}')".format(
            self.__class__.__name__,
            self.genre,
            self.name,
        )

    def __str__(self):
        return self.name

    @property
    def genre(self):
        return self._genre

    @genre.setter
    def genre(self, genre):
        self._genre = genre

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


class RollingStones(Band):
    NAME = 'Rolling Stones'

    def __init__(self, *args, **kwargs):
        super().__init__(name=self.NAME, *args, **kwargs)


class RedHotChiliPeppers(Band):
    NAME = 'Red Hot Chili Peppers'

    def __init__(self, *args, **kwargs):
        super().__init__(name=self.NAME, *args, **kwargs)


if __name__ == '__main__':
    rs = RollingStones()
    rhcp = RedHotChiliPeppers('funk')

    print("{}, {}.".format(rs, rs.genre))
    print("{}, {}.".format(rhcp, rhcp.genre))
