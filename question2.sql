-- postgresql 9.0+
-- build test db
drop table if exists campaigns cascade;
create table if not exists campaigns (
	id serial primary key,
	name varchar
);

drop table if exists venue_types cascade;
create table if not exists venue_types (
	id serial primary key,
	name varchar
);

drop table if exists tags cascade;
create table if not exists tags (
	id serial primary key,
	unique_identifier varchar,
	venue_type_id integer,
	foreign key (venue_type_id) references venue_types(id) on delete cascade
);

drop table if exists campaign_tag cascade;
create table if not exists campaign_tag (
	id serial primary key,
	campaign_id integer,
	tag_id integer,
	foreign key (campaign_id) references campaigns(id) on delete cascade,
	foreign key (tag_id) references tags(id) on delete cascade
);

-- mock data
INSERT INTO campaigns (id, name) VALUES
  (1, 'campaign one'),
	(2, 'campaign two'),
	(3, 'campaign three'),
	(4, 'campaign four');

INSERT INTO venue_types (id, name) VALUES
  (1, 'venue type 1'),
	(2, 'venue type 2'),
	(3, 'venue type 3');

INSERT INTO tags (id, unique_identifier, venue_type_id) VALUES
  (1, 'asdf1', 1),
	(2, 'asdf2', 1),
	(3, 'qqq', 3),
	(4, 'www', 3);

INSERT INTO campaign_tag (id, campaign_id, tag_id) VALUES
  (1, 1, 1),
	(2, 1, 3),
	(3, 2, 3),
	(4, 3, 2);

-- query
select * from campaign_tag;
select * from campaigns;
select * from tags;

-- to see the campaigns result as ordered list, postgres 9.0+ only. Otherwise we have to use subquery
select string_agg(distinct(c.name), ',' order by c.name) as venue_type__campaign
from campaign_tag ct
join tags t
  on ct.tag_id = t.id
join campaigns c
  on ct.campaign_id = c.id
where t.venue_type_id in (1, 3);

-- to see the campaigns result as unordered list
select string_agg(distinct(c.name), ',') as venue_type__campaigns
from campaign_tag ct
join tags t
  on ct.tag_id = t.id
join campaigns c
  on ct.campaign_id = c.id
where t.venue_type_id in (1, 3);

-- test 1 missing t.venue_type_id
-- to see the campaigns result table
select t.venue_type_id, ct.campaign_id, c.name from campaign_tag ct
join tags t
  on ct.tag_id = t.id
join campaigns c
  on ct.campaign_id = c.id
where t.venue_type_id in (1, 2);

-- test 2 duplicate c.name
-- to see the campaigns result table
select t.venue_type_id, ct.campaign_id, c.name from campaign_tag ct
join tags t
  on ct.tag_id = t.id
join campaigns c
  on ct.campaign_id = c.id
where t.venue_type_id in (1, 3);

-- test 3 no result
-- to see the campaigns result table
select t.venue_type_id, ct.campaign_id, c.name from campaign_tag ct
join tags t
  on ct.tag_id = t.id
join campaigns c
  on ct.campaign_id = c.id
where t.venue_type_id in (2);

-- to also see missing venue types
select vt.id, vt.name, ct.campaign_id, c.name  from venue_types vt
full outer join tags t
  on t.venue_type_id = vt.id
full outer join campaign_tag ct
  on ct.tag_id = t.id
full outer join campaigns c
  on ct.campaign_id = c.id
where vt.id in (1, 2);
